/*function myFunction() {
  
  var leaugeView = document.getElementById("leaugeView"); 
  if (leaugeView.style.visibility === "visible") {
    leaugeView.style.visibility = "hidden";
  } else {
    leaugeView.style.visibility = "visible";
  }
}*/

$(function () {

  loadData("competitions/2002", ".bundesliga", "competitions-template");
  //loadData("competitions/2021", ".premier", "competitions-template");
  //loadData("competitions/2016", ".championship", "competitions-template");
  /*loadData("competitions/2019", ".serieA", "competitions-template");
  loadData("competitions/2001", ".uefaCha", "competitions-template");
  loadData("competitions/2000", ".fifa", "competitions-template");
  loadData("competitions/2013", ".seria", "competitions-template");
  loadData("competitions/2018", ".euroCha", "competitions-template");
  loadData("competitions/2015", ".ligue", "competitions-template");
  loadData("competitions/2003", ".eredivise", "competitions-template");
  loadData("competitions/2017", ".primeira", "competitions-template");
  loadData("competitions/2014", ".primera", "competitions-template");
  */

  //loadData("matches?competitions=2002&dateFrom=" + formatDate(dateFrom) + "&dateTo=" + formatDate(dateTo), ".col3", "matches-template");






});


var id = "";

function loadData(suffix, pos, templates) {
  $.ajax({
    method: "GET",
    headers: {
      "X-Auth-Token": "e32d2c6f551d4093bd5f110b8619cf83"
    },
    url: "http://api.football-data.org/v2/" + suffix,
    success: function (result) {

      var obj = {};
      if (templates == "table-template") {
        obj.result = result.standings[0].table;
      }
      else if (templates == "competitions-template") {
        obj.result = result; //.competitions;
      }
      else if (templates == "matches-template") {
        obj.result = result.matches;
      }
      else if (templates == "teams-template") {
        obj.result = result.teams;
      }
      else if (templates == "team-detail") {
        obj.teamname = result;
        obj.result = result.squad;
      }
      else if (templates == "player-detail") {
        obj.result = result;
      }

      var source = $("#" + templates).html();
      var template = Handlebars.compile(source);
      var output = template(obj);
      $(pos).html(output);

      /*$(".teams tr").click(function (event) {
        console.log(event.currentTarget.attributes[0].nodeValue);
        alert("hallo i bims");
      })*/


      //console.log(event.currentTarget.attributes[0].nodeValue);
      switch (pos) {
        case ".bundesliga":
          $(pos + " tr").click(function (event) {
            //console.log(event.currentTarget.attributes[0].nodeValue);
            loadData("competitions/" + event.currentTarget.attributes[0].nodeValue + "/teams", ".teams", "teams-template");
            var dateFrom = new Date();
            var dateTo = new Date;
            dateTo.setDate(dateTo.getDate() + 10);
            loadData("matches?competitions=" + event.currentTarget.attributes[0].nodeValue + "&dateFrom=" + formatDate(dateFrom) + "&dateTo=" + formatDate(dateTo), ".col3", "matches-template");
            $(".col2").show("slow");
            $(".col3").show("slow");
            id = event.currentTarget.attributes[0].nodeValue;
          });
        case ".premier":
          $(pos + " tr").click(function (event) {
            loadData("competitions/" + event.currentTarget.attributes[0].nodeValue + "/teams", ".teams", "teams-template");
            var dateFrom = new Date();
            var dateTo = new Date;
            dateTo.setDate(dateTo.getDate() + 10);
            loadData("matches?competitions=" + event.currentTarget.attributes[0].nodeValue + "&dateFrom=" + formatDate(dateFrom) + "&dateTo=" + formatDate(dateTo), ".col3", "matches-template");
            $(".col2").show("slow");
            $(".col3").show("slow");
            id = event.currentTarget.attributes[0].nodeValue;
          });
        case ".championship":
          $(pos + " tr").click(function (event) {
            loadData("competitions/" + event.currentTarget.attributes[0].nodeValue + "/teams", ".teams", "teams-template");
            var dateFrom = new Date();
            var dateTo = new Date;
            dateTo.setDate(dateTo.getDate() + 10);
            loadData("matches?competitions=" + event.currentTarget.attributes[0].nodeValue + "&dateFrom=" + formatDate(dateFrom) + "&dateTo=" + formatDate(dateTo), ".col3", "matches-template");
            $(".col2").show("slow");
            $(".col3").show("slow");
            id = event.currentTarget.attributes[0].nodeValue;
          });
        case ".teams":
          $(".teams tr").click(function (event) {
            //alert("ka");
            console.log(event.currentTarget.attributes[0].nodeValue);
            $(".col1").hide("slow");
            $(".col2").hide("slow");
            $(".col3").hide("slow");
            loadData("teams/" + event.currentTarget.attributes[0].nodeValue, ".teamDetail", "team-detail");

            $(".row1").show("slow");

          });
        case ".col3":
          $(".col3 td").click(function (event) {
            //alert("ka");
            console.log(event.currentTarget.attributes[0].nodeValue);
            $(".col1").hide("slow");
            $(".col2").hide("slow");
            $(".col3").hide("slow");
            loadData("teams/" + event.currentTarget.attributes[0].nodeValue, ".teamDetail", "team-detail");
            $(".row1").show("slow");
          });
        case ".teamDetail":
          $(".teamDetail tr").click(function (event) {
            //alert("ka");
            console.log(event.currentTarget.attributes[0].nodeValue);
            loadData("players/" + event.currentTarget.attributes[0].nodeValue, ".playerDetail", "player-detail");
            $(".row2").show("slow");
          });
        // default:
        // console.log("ka");
      }





      /*if(pos== ".col2"){
        $(pos + " tr").click(function (event) {
          console.log(event.currentTarget.attributes[0].nodeValue);
          
          
          
        
        })
      }*/


    },
    error: function (error) {
      alert("Error:" + error);
    }
  });
}

function loadTeamData() {
  loadData("competitions/" + id + "/teams", ".teams", "teams-template");
}

function loadTable() {
  loadData("competitions/" + id + "/standings", ".teams", "table-template");
}

function backToMenu() {
  $(".row1").hide("slow");
  if ($(".row2").is(":visible")) {
    $(".row2").hide("slow");
  }

  $(".col1").show("slow");
  $(".col2").show("slow");
  $(".col3").show("slow");
}



function formatDate(date) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('-');
}
/*
$.ajax({
  method: "GET",
  headers: {
    "X-Auth-Token": "e32d2c6f551d4093bd5f110b8619cf83"
  },
  url: "http://api.football-data.org/v2/competitions",
  success: function (result) {
    console.log(result);
    var output = "<h1> Competitions </h1>";
    result.competitions.forEach(element => {
      output += "<div>" + element.name + "</div>";
      //console.log(element.position + " " + element.team.name);
      console.log(element.name);
    });
    //output += "</ol>";
    $(".col1").html(output);
  },
  error: function (error) {
    alert("Error:" + error);
  }
});
*/

